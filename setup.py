from setuptools import setup, find_packages


with open('README.md') as f:
    readme = f.read()

setup(
    name='brdmd',
    version='0.1.0',
    description='Python tools various things',
    long_description=readme,
    author='Brent Styles et al',
    author_email='brentstyles18@gmail.com',
    include_package_data=True,
    packages=find_packages(exclude=('figures','notebooks','data')),
    python_requires='>=3.6'
)
