{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "086019b9",
   "metadata": {},
   "source": [
    "# Dynamic Mode Decomposition\n",
    "----\n",
    "## Overview\n",
    "DMD takes a dataset $\\mathbf{X}(t)$ and puts it in the form $\\mathbf{X}(t)\\approx \\sum^{r}_{k=1}\\phi_k e^{\\omega_kt}b_k$ where the term $\\phi_k$ denotes each DMD mode, and the rest of the terms constitute the time dynamics of the data.\n",
    "## Creating Problem\n",
    "This code combines two periodic signals together and generates some data so that DMD can be performed on it. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "5f7c5f19",
   "metadata": {},
   "outputs": [],
   "source": [
    "import matplotlib.pyplot as plt\n",
    "import numpy as np\n",
    "import scipy\n",
    "import os, sys\n",
    "\n",
    "basedir = os.getcwd()\n",
    "savedir = os.path.join(basedir,'../figures/example')\n",
    "\n",
    "# Plot parameters\n",
    "plt.rc('font', family='serif')\n",
    "plt.rcParams.update({'font.size': 20,\n",
    "                     'lines.linewidth': 2,\n",
    "                     'axes.labelsize': 16, # fontsize for x and y labels (was 10)\n",
    "                     'axes.titlesize': 20,\n",
    "                     'xtick.labelsize': 16,\n",
    "                     'ytick.labelsize': 16,\n",
    "                     'legend.fontsize': 16,\n",
    "                     'axes.linewidth': 2})"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "0bd6a2d2",
   "metadata": {},
   "outputs": [],
   "source": [
    "n = 200                                                     ## number of values\n",
    "m = 80                                                      ## number of snapshots\n",
    "x = np.linspace(-15,15,n)                                   ## create x axis\n",
    "t = np.linspace(0,8*np.pi,m)                                ## create t axis\n",
    "dt = t[1]-t[0]                                              \n",
    "\n",
    "Xgrid, T = np.meshgrid(x, t)\n",
    "f1 = 0.5*np.cos(Xgrid)*(1+0*T)\n",
    "f2 = (np.tanh(Xgrid)/np.cosh(Xgrid))*(2*np.exp(1j*2.8*T))\n",
    "f = f1 + f2\n",
    "f = np.transpose(f)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "ac4d4253",
   "metadata": {},
   "outputs": [],
   "source": [
    "print('# of pts:',n)\n",
    "print('# of timesteps:',m)\n",
    "print('%f <= t <= %f'%(t[0],t[-1]))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "080c9c86",
   "metadata": {},
   "outputs": [],
   "source": [
    "def rel_err(true,recon):\n",
    "    return np.linalg.norm(true-recon)/np.linalg.norm(true)\n",
    "\n",
    "def ss_err(true,recon):\n",
    "    t = true.shape[-1]\n",
    "    ind=[slice(None)]*true.ndim\n",
    "    err = np.zeros(t)\n",
    "    for i in range(0,t):\n",
    "        ind[-1] = i\n",
    "        err[i] = rel_err(true[tuple(ind)],recon[tuple(ind)])\n",
    "    return err\n",
    "\n",
    "def plot_err(ax,true,recon,ls='-'):\n",
    "    ax.plot(ss_err(true,recon),ls)\n",
    "\n",
    "def save_figure(fig,name):\n",
    "    fig.savefig(os.path.join(savedir,name))\n",
    "    print('Figure saved on disk with name:',name)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "977aaca7",
   "metadata": {},
   "source": [
    "## SVD and creating DMD modes\n",
    "The idea behind dynamic mode decomposition is that a complicated function $\\frac{d\\vec{y}}{dt}=f(\\vec{y},t,\\mu)$ can be represented by a linear approximation of it: $\\vec{x}'=\\mathbf{A}\\vec{x}$. By taking a high fidelity model of a complicated function at different time intervals in the matrix $\\mathbf{X}$, the linear approximation can be written as: \n",
    "$$\\mathbf{X}'=\\mathbf{A}\\mathbf{X}$$\n",
    "Multiplying both sides by the pseudoinverse of $\\mathbf{X}$ yields: \n",
    "$$\\mathbf{A}=\\mathbf{X}'\\mathbf{X}^\\dagger$$\n",
    "Take the SVD of $\\mathbf{X}$:\n",
    "$$\\mathbf{X} = \\mathbf{U}\\mathbf{\\Sigma} \\mathbf{V}^*$$\n",
    "Use this formulation to find the pseudoinverse of $\\mathbf{X}$ and perform a rank $r$ truncation on the SVD of $\\mathbf{X}$:\n",
    "$$\\mathbf{X}^\\dagger=\\mathbf{V}_r{\\mathbf{\\Sigma}_r}^{-1}{\\mathbf{U}_r}^*$$\n",
    "$$\\mathbf{A}=\\mathbf{X}'\\mathbf{V}_r{\\mathbf{\\Sigma}_r}^{-1}{\\mathbf{U}_r}^*$$\n",
    "Find a truncated version of the matrix $\\mathbf{A}$ by projecting it on to the POD modes:\n",
    "$$\\mathbf{A}_r = {\\mathbf{U}_r}^*\\mathbf{A}\\mathbf{U}_r={\\mathbf{U}_r}^*\\mathbf{X}'\\mathbf{V}_r{\\mathbf{\\Sigma}_r}^{-1}$$\n",
    "Find the eigenvalues $\\mathbf{\\Lambda}$ and the eigenvectors $\\mathbf{W}$ of $\\mathbf{A}_r$:\n",
    "$$\\mathbf{A}_r\\mathbf{W}=\\mathbf{W}\\mathbf{\\Lambda}$$\n",
    "$$\\mathbf{\\Phi}=\\mathbf{X}'\\mathbf{V}_r{\\mathbf{\\Sigma}_r}^{-1}\\mathbf{W}$$\n",
    "$\\mathbf{\\Phi}$ contains the exact DMD modes\n",
    "\n",
    "(note that another expression for DMD modes known as the projected DMD modes is given by: $\\mathbf{\\Phi}=\\mathbf{U}\\mathbf{W}$)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "9b043559",
   "metadata": {},
   "outputs": [],
   "source": [
    "x1 = f[:,0:m-1]\n",
    "x2 = f[:,1:m]\n",
    "u, s, vh = np.linalg.svd(x1)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "37bb50ad",
   "metadata": {},
   "outputs": [],
   "source": [
    "r=2\n",
    "plt.semilogy(s/np.sum(s),'.k') # Plot singular values\n",
    "plt.semilogy(r-1,s[r-1]/sum(s),'.r')\n",
    "plt.title('Singular Values')\n",
    "\n",
    "save = False\n",
    "if save:\n",
    "    save_figure(plt,'example_svals_r=%d.pdf'%(r))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "b9bd4d23",
   "metadata": {},
   "outputs": [],
   "source": [
    "u_t = u[:,0:r]\n",
    "s_t = np.zeros((r,r))\n",
    "np.fill_diagonal(s_t,s[0:r])\n",
    "vh_t = vh[0:r,:]\n",
    "v_t = np.conjugate(vh_t).T\n",
    "uh_t = np.conjugate(u_t).T\n",
    "s_tinv = np.linalg.inv(s_t)\n",
    "A_til = uh_t @ x2 @ v_t @ s_tinv\n",
    "l, w = np.linalg.eig(A_til)\n",
    "phi = x2 @ v_t @ s_tinv @ w"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "f9e004f6",
   "metadata": {},
   "source": [
    "## Finding time dynamics and reconstruction\n",
    "$\\mathbf{\\Lambda}$ is a diagonal matrix containing the eigenvalues, $\\lambda_k$, of $\\mathbf{A}_r$. To get the value $\\omega_k$ needed in reconstructing the time dynamics $e^{\\omega_kt}b_k$, the discrete eigenvalues $\\lambda_k$ need to be converted to continuous ones $\\omega_k$ through the formula:\n",
    "$$\\omega_k=ln(\\lambda_k)/{\\Delta}t$$\n",
    "To solve for $\\mathbf{b}$, look at where $t=0$:\n",
    "$$\\mathbf{b}=\\mathbf{\\Phi}^{\\dagger}x_1$$\n",
    "where $x_1$ is the first column of the matrix $\\mathbf{X}$"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "6fcd5116",
   "metadata": {},
   "outputs": [],
   "source": [
    "omega = np.log(l)/dt\n",
    "\n",
    "b = np.linalg.pinv(phi) @ f[:,0]\n",
    "b.reshape((r,1))\n",
    "timedyn = np.zeros((r,m),dtype = 'complex')\n",
    "\n",
    "for i in range(0,m):\n",
    "    timedyn[:,i] = np.exp(omega*t[i])*b\n",
    "f_new = phi@timedyn"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "494eaebb",
   "metadata": {},
   "outputs": [],
   "source": [
    "slc = 79\n",
    "fig, axs = plt.subplots(1,2,figsize=(16,4))\n",
    "axs[0].plot(x,np.real(f[:,slc]),'-b')\n",
    "axs[1].plot(x,np.real(f_new[:,slc]),'-r')\n",
    "axs[0].set_title('Added periodic signals at t = %.2f'%(t[slc]))\n",
    "axs[1].set_title('Exact DMD reconstruction at t = %.2f'%(t[slc]))\n",
    "\n",
    "save = False\n",
    "if save:\n",
    "    save_figure(plt,'example_recon_t=%.2f_r=%d.pdf'%(t[slc],r))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "89d92496",
   "metadata": {},
   "outputs": [],
   "source": [
    "plot_err(plt,f,f_new,ls='r.')"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "2b9481e8",
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.8.10"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
